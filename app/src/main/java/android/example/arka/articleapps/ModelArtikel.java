package android.example.arka.articleapps;

public class ModelArtikel {
    String penerbit, judul, artikel, created_at;

    public ModelArtikel(String penerbit, String judul, String artikel, String created_at) {
        this.penerbit = penerbit;
        this.judul = judul;
        this.artikel = artikel;
        this.created_at = created_at;
    }

    public String getPenerbit() {
        return penerbit;
    }

    public void setPenerbit(String penerbit) {
        this.penerbit = penerbit;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getArtikel() {
        return artikel;
    }

    public void setArtikel(String artikel) {
        this.artikel = artikel;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
