package android.example.arka.articleapps;

import android.content.Context;
import android.content.Intent;
import android.example.arka.articleapps.R;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class AdapterArtikel extends RecyclerView.Adapter <AdapterArtikel.Myviewholder> {
    private Context context;
    private List <ModelArtikel> list;

    public AdapterArtikel(Context context, List<ModelArtikel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public Myviewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new Myviewholder(LayoutInflater.from(context).inflate(R.layout.artikel,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull Myviewholder myviewholder, int i) {
        final ModelArtikel apakek = list.get(i);
        myviewholder.nama.setText(apakek.penerbit);
        myviewholder.judul.setText(apakek.judul);
        myviewholder.deskripsi.setText(apakek.artikel);

        myviewholder.apakeknamanya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, DetailArticleActivity.class);
                i.putExtra("penerbit", apakek.penerbit + ", " + apakek.created_at);
                i.putExtra("artikel", apakek.artikel);
                i.putExtra("judul", apakek.judul);
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class Myviewholder extends RecyclerView.ViewHolder{
        private TextView nama, judul, deskripsi;
        private ConstraintLayout apakeknamanya;

        public Myviewholder(@NonNull View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.nama);
            judul = itemView.findViewById(R.id.judul);
            deskripsi = itemView.findViewById(R.id.deskripsi);
            apakeknamanya=itemView.findViewById(R.id.namaapa);
        }
    }
}
