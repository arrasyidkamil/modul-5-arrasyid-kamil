package android.example.arka.articleapps;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.example.arka.articleapps.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    SharedPreferences spNight;

    final String PREF_NIGHT_MODE = "NightMode";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        spNight = getSharedPreferences(PREF_NIGHT_MODE , Context.MODE_PRIVATE);

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.nigatheme);
        }else{
            setTheme(R.style.AppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(spNight.getBoolean(PREF_NIGHT_MODE,false)){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);

            TextView textView = findViewById(R.id.textView);
            textView.setText("Good Night");
        }
    }

    public void listarticle(View view) {
        Intent list = new Intent(MainActivity.this,ItemArtikel.class);
        startActivity(list);
    }

    public void createarticle(View view) {
        Intent create = new Intent(MainActivity.this, CreateArticle.class);
        startActivity(create);
    }

    public void setting(View view) {
        Intent setting = new Intent(MainActivity.this, Setting.class);
        startActivity(setting);
    }
}
