package android.example.arka.articleapps;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.example.arka.articleapps.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class CreateArticle extends AppCompatActivity {

    EditText editTextTitle, editTextArticle, editTextAuthor;
    Button btnSave;
    private DataHelper db;
    String mTitle, mArticle, mAuthor;

    SharedPreferences spNight;

    final String PREF_NIGHT_MODE = "NightMode";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        spNight = getSharedPreferences(PREF_NIGHT_MODE , Context.MODE_PRIVATE);

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.nigatheme);
        }else{
            setTheme(R.style.AppTheme);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_article);

        editTextArticle = findViewById(R.id.editText2);
        editTextTitle = findViewById(R.id.editText);
        editTextAuthor = findViewById(R.id.editText3);
        btnSave = findViewById(R.id.button);
        db = new DataHelper(this);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postArticle();
            }
        });
    }

    private void postArticle() {
        mTitle = editTextTitle.getText().toString().trim();
        mArticle = editTextArticle.getText().toString().trim();
        mAuthor = editTextAuthor.getText().toString().trim();

        if (mTitle.isEmpty()){
            msg("Title kosong");
            return;
        }
        if (mArticle.isEmpty()){
            msg("Article kosong");
            return;
        }
        if (mAuthor.isEmpty()){
            msg("Author kosong");
            return;
        }

        saveToDb();

    }

    private void saveToDb() {
        if (db.insertData(new ModelArtikel(mAuthor, mTitle, mArticle, getDateTime()))){
            msg("Data insyallah berhasil ditambahkan");
            startActivity(new Intent(CreateArticle.this, ItemArtikel.class));
        }else {
            msg("Something wrong");
        }
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "EEE d MMM `yy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    private void msg (String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
