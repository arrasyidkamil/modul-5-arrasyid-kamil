package android.example.arka.articleapps;

import android.content.Context;
import android.content.SharedPreferences;
import android.example.arka.articleapps.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class ItemArtikel extends AppCompatActivity {
    RecyclerView bebas;
    AdapterArtikel terserah;
    List <ModelArtikel> list;
    DataHelper dbHelper;

    SharedPreferences spNight;

    final String PREF_NIGHT_MODE = "NightMode";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        spNight = getSharedPreferences(PREF_NIGHT_MODE , Context.MODE_PRIVATE);

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.nigatheme);
        }else{
            setTheme(R.style.AppTheme);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_artikel);

        dbHelper = new DataHelper(this);
        bebas=findViewById(R.id.recy);
        bebas.setLayoutManager(new LinearLayoutManager(this));

        list = new ArrayList<>();
        terserah = new AdapterArtikel(this, list);
        bebas.setAdapter(terserah);

        dbHelper.readData(list);

        terserah.notifyDataSetChanged();
    }
}
