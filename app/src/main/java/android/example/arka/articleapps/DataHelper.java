package android.example.arka.articleapps;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.List;

public class DataHelper extends SQLiteOpenHelper {

    private Context context;
    SQLiteDatabase db;

    public static final String DATABASE_NAME = "artikel.db";
    public static final String TABLE_NAMA = "artikel_table";
    private static final int DATABASE_VERSION = 2;

    public static final String COL_1 = "judul";
    public static final String COL_2 = "Artikel";
    public static final String COL_3 = "penerbit";


    public DataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        db = this.getWritableDatabase();
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table artikel_table (id integer primary key autoincrement,"+
                "judul text," +
                "artikel text, " +
                        "penerbit text, " + "created_at datetime);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS artikel_table");

        // create new tables
        onCreate(db);
    }

    public boolean insertData(ModelArtikel article){
        ContentValues val = new ContentValues();
        val.put("penerbit", article.getPenerbit());
        val.put("judul", article.getJudul());
        val.put("artikel", article.getArtikel());
        val.put("created_at", article.getCreated_at());

        long res = db.insert("artikel_table", null, val);
        return res != -1;
    }

    public void readData(List<ModelArtikel> dataArticle){
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT penerbit, judul, artikel, created_at FROM artikel_table", null);
        while (cursor.moveToNext()){
            dataArticle.add(new ModelArtikel(cursor.getString(0), cursor.getString(1), cursor.getString(2), cursor.getString(3)));
        }
        cursor.close();
    }
}