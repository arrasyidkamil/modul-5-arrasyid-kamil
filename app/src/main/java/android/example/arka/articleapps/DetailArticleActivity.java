package android.example.arka.articleapps;

import android.content.Context;
import android.content.SharedPreferences;
import android.example.arka.articleapps.R;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.TextView;

public class DetailArticleActivity extends AppCompatActivity {
    TextView penulis, judul, deskripsi, notif;
    final String PREF_FONT_SIZE = "BigSize";
    SharedPreferences spFont;
    SharedPreferences spNight;

    final String PREF_NIGHT_MODE = "NightMode";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        spNight = getSharedPreferences(PREF_NIGHT_MODE , Context.MODE_PRIVATE);

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            setTheme(R.style.nigatheme);
        }else{
            setTheme(R.style.AppTheme);

            if(spNight.getBoolean(PREF_NIGHT_MODE,false)){
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            }
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_article);

        penulis = findViewById(R.id.tv_author_detail);
        judul = findViewById(R.id.tv_title_detail);
        deskripsi = findViewById(R.id.tv_desc_detail);
        notif = findViewById(R.id.tv_notif_size_detail);

        if (getIntent() != null){
            penulis.setText(getIntent().getStringExtra("penerbit"));
            judul.setText(getIntent().getStringExtra("artikel"));
            deskripsi.setText(getIntent().getStringExtra("judul"));
        }
        spFont = getSharedPreferences(PREF_FONT_SIZE, Context.MODE_PRIVATE);
        if (spFont.getBoolean(PREF_FONT_SIZE, false)){
            deskripsi.setTextSize(24);
            notif.setVisibility(View.VISIBLE);
        }else{
            notif.setVisibility(View.INVISIBLE);
            deskripsi.setTextSize(16);
        }
    }
}
